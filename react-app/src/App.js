import React, { useState } from 'react';
import './App.css';
import { Container } from 'react-bootstrap';

import AppNavbar from './components/AppNavbar'
import Error from './components/Error';


import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import SpecificProduct from './pages/SpecificProduct';

import { UserProvider } from './UserContext'

//for routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

function App() {


    // React context is nothing but a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested. Context helps you broadcast data and changes happening to that data/state to all components.

    const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

    //Function for clearing localStorage on logout
    const unsetUser = () => {
          localStorage.clear()
    }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/products' element={<Products />} />
            <Route path='/register' element={<Register />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path="/products/:productId" element={<SpecificProduct />} />
            <Route path='*' element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  );
}


export default App;
