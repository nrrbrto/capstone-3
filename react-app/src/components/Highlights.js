import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
    return (
    <Row>
        <Col xs={12} md={4}>
        
        <Card className='cardHighlight p-3'>
            <Card.Body>
                <Card.Title>
                    <h2>Discounted(?) Book 1</h2>
                </Card.Title>
                <Card.Text> 
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla animi suscipit, consequuntur fugiat tempore ipsam neque expedita! Laudantium reiciendis tenetur cumque velit molestias cupiditate ab nostrum! Non natus iste optio?  
                </Card.Text>
            </Card.Body>
        </Card>    
        </Col>

        <Col xs={12} md={4}>
        <Card className='cardHighlight p-3'>
        <Card.Body>
                <Card.Title>
                    <h2>Discounted(?) Book 2</h2>
                </Card.Title>
                <Card.Text> 
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla animi suscipit, consequuntur fugiat tempore ipsam neque expedita! Laudantium reiciendis tenetur cumque velit molestias cupiditate ab nostrum! Non natus iste optio?  
                </Card.Text>
            </Card.Body>
        </Card>
            
        </Col>

        <Col xs={12} md={4}>
        <Card className='cardHighlight p-3'>
        <Card.Body>
                <Card.Title>
                    <h2>Discounted(?) Book 3</h2>
                </Card.Title>
                <Card.Text> 
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla animi suscipit, consequuntur fugiat tempore ipsam neque expedita! Laudantium reiciendis tenetur cumque velit molestias cupiditate ab nostrum! Non natus iste optio?  
                </Card.Text>
            </Card.Body>
        </Card>         
        </Col>
    </Row>
    )
    

}