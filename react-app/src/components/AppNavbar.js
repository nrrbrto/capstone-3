import React, { useContext} from 'react';
import {Navbar,Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import UserContext from "../UserContext";

export default function AppNavbar() {

    //State to store user info in login page
    const {user} = useContext(UserContext);

    return(
        <Navbar bg="dark" expand="lg" variant="dark">
            <Navbar.Brand href="#home" className='ms-2' as={Link} to="/" >Liber Mercatus</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className='ms-auto'>
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/products">Buy Books</Nav.Link>


                    { (user.accessToken !== null) ?
                        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                        :
                        <>
                            <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            <Nav.Link as={Link} to="/login">Login</Nav.Link>
                        </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}