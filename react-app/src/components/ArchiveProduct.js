import React, { } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({ product, isActive, fetchData }) {

    const archiveToggle = (productId) => {
        fetch(`https://ecommerce-csp2.herokuapp.com/products/archive/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
            .then(data => {
                console.log(data)
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully enabled'
                    })
                    fetchData();
				})
    }

    const unArchiveToggle = (productId) => {
        fetch(`https://ecommerce-csp2.herokuapp.com/products/activate/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
            .then(data => {
                console.log(data)
                    Swal.fire({
                        title: 'Success',
                        icon: 'success',
                        text: 'Product successfully enabled'
                    })
                    fetchData();
				})
    }

    return (
        <>
            {isActive ?

                <Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

                :

                <Button variant="success" size="sm" onClick={() => unArchiveToggle(product)}>Unarchive</Button>

            }
        </>

    )
}