import React from 'react';
import {Row,Col, Button} from 'react-bootstrap';
import { Link } from "react-router-dom"

export default function Banner() {
    return(
        <Row>
            <Col className='p-5'>
                <h1 className='mb-3'>Welcome!</h1>
                <p>Find your new book from our wide assortment, as well as drawn posters. Feel free to look around.</p>
                <Button variant="primary" as={ Link } to="/products"> Buy now! </Button>  
            </Col>
        </Row>    
    )
}