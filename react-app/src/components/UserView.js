import React, {useState, useEffect} from "react"
import ProductCard from './ProductCard'

export default function UserView({productData}){ //nasa curly braces kasi un[packed]
    const [product, setProduct] = useState();

    useEffect(() =>{
        
        const productArr = productData.map(product => {
            //only active courses
            if (product.isActive === true) {
                return (
                    <ProductCard productProp={product} key={product._id}/>
                )
            } else {
                return null
            }
        })
        setProduct(productArr)
    }, [productData])
    

    return (
        <>
            {product}
        </>
    )
}
