import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Register() {

	const { user } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== "" && gender !== "" && mobileNumber !== "") && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, gender, mobileNumber])

	function registerUser(e) {
		//Prevents page redirection via form submission
		e.preventDefault();
		fetch('https://ecommerce-csp2.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1,
				checkPass: password2,
				firstName: firstName,
				lastName: lastName,
				gender: gender,
				mobileNo: mobileNumber
			})
		})
			//Clear input fields
			setEmail('');
			setPassword1('');
			setPassword2('');
	
			Swal.fire({
				title: "Yaaaaaaaaay!",
				icon: "success",
				text: "You have successfully registered"
			})
	}

	return (

		//Conditional rendering
		(user.accessToken !== null) ? 

		<Navigate to="/" />

		:

		<Form onSubmit={(e) => registerUser(e)} className='py-3'>
			<Form.Group  > 
			<h1>Register</h1>
				<Form.Label>Email Adress</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else
					</Form.Text>
			</Form.Group>

			<Form.Group> 
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					/>
			</Form.Group>

			<Form.Group> 
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Confirm Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					/>
			</Form.Group>

			<Form.Group> 
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="first name"
					placeholder="Enter First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					/>
			</Form.Group>
			<Form.Group> 
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="last name"
					placeholder="Enter Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					/>
			</Form.Group>
			<Form.Group> 
				<Form.Label>Gender</Form.Label>
				<Form.Control 
					type="gender"
					placeholder="Enter your preferred gender"
					required
					value={gender}
					onChange={e => setGender(e.target.value)}
					/>
			</Form.Group>
			<Form.Group> 
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter your mobile number"
					required
					value={mobileNumber}
					onChange={e => setMobileNumber(e.target.value)}
					/>
			</Form.Group>

			
			{isActive ? 
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>

				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

			}
			
		</Form>












		)
}
